package br.com.senac.ex6;

public class Valores {

    private int qtd;
    private double precoItem;

    public Valores() {
    }

    public Valores(int qtd, double precoItem) {
        this.qtd = qtd;
        this.precoItem = precoItem;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public double getPrecoItem() {
        return precoItem;
    }

    public void setPrecoItem(double precoItem) {
        this.precoItem = precoItem;
    }

}
