
package br.com.senac.ex6.teste;

import br.com.senac.ex6.ClasseRefatorada;
import br.com.senac.ex6.Valores;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClasseRefatoradaTest {
    
    @Test
    public void codigoRefatorado(){
        
        Valores valor = new Valores(2, 300);
        ClasseRefatorada calculadora = new ClasseRefatorada();
        
        double resultado = calculadora.getPreco(valor);
        assertEquals(0.98, resultado, 0.001);
    }
    
}
